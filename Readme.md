# Steps to start
## 1. Clone git-repo: "git clone https://gitlab.com/Benjamin_Guggenberger/reactivesystem.git"
## 2. in folder with 'docker-compose.yml', execute "docker compose up" in command line
## 3. in folder './graphql-client' execute "npm start" in command line to start client
## The REST Api can be called via POST Request at 'http://localhost:8081/api/rest/createPayment' - body form: {"amount": 1, "description": "x", "creditor_iban":"x", "debtor_iban":"x"}